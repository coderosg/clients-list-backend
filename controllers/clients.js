const Client = require('../models/Client')

exports.getClients = (req, res, next) => {
    const {is_completed} = req.query,
        page = req.query.page || 1
    if (is_completed) {
        const isCompletedBoolean = JSON.parse(is_completed)
        Client.fetchByCompletion(isCompletedBoolean, page)
            .then(clients => {
                res.status(200).json(clients)
            })
    } else {
        Client.fetchAll(page)
            .then(clients => {
                res.status(200).json(clients)
            })
    }

};

exports.getClient = (req, res, next) => {
    Client.fetchById(req.params.clientId)
        .then(client => {
            res.status(200).json(client)
        })
};

exports.postClient = (req, res, next) => {
    const name = req.body.name,
        date = req.body.date,
        time = req.body.time,
        service = req.body.service,
        price = req.body.price,
        vk = req.body.vk,
        phone = req.body.phone,
        is_completed = req.body.is_completed

    const client = new Client(name, date, time, service, price, vk, phone, is_completed)

    return client.save(req.body._id)
        .then(() => {
            Client.fetchAll().then(clients => {
                res.json(clients)
            })
        })
};

exports.deleteClient = (req, res, next) => {
    const {clientId} = req.params
    Client.deleteById(clientId)
        .then(() => {
            Client.fetchAll()
                .then(clients => {
                    res.status(200).json(clients)
                })
                .catch(err => {
                    console.log(err);
                })

        })
        .catch(err => {
            console.log(err);
        })
};

exports.finishWork = (req, res, next) => {
    const {clientId} = req.params
    Client.finishWork(clientId)
        .then(() => {
            Client.fetchByCompletion(false)
                .then(clients => {
                    res.status(200).json(clients)
                })
                .catch(err => {
                    console.log(err);
                })

        })
        .catch(err => {
            console.log(err);
        })
}