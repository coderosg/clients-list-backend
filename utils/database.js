const mongodb = require('mongodb');

const MongoClient = mongodb.MongoClient;

let db;

const mongodbConnect = (callback) => {
    MongoClient.connect(
        `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0-lodx1.mongodb.net/janes-clients?retryWrites=true&w=majority`,
        {useNewUrlParser: true}
    )
        .then(client => {
            console.log('Connected');
            db = client.db()
            callback()
        })
        .catch(err => {
            throw err;
        })
};

const getDb = () => {
    if (db) {
        return db
    } else {
        throw 'No database'
    }
}

exports.mongoConnect = mongodbConnect
exports.getDb = getDb