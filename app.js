const express = require('express')
const path = require('path')

const app = express()

const bodyParser = require('body-parser')
const helmet = require('helmet')

const mongodbConnect = require('./utils/database').mongoConnect

const port = process.env.PORT || 3000

const clientsRouter = require('./routes/clients')


app.disable('etag');

app.use(bodyParser.json())

app.use(express.static(path.resolve(__dirname, 'public')))

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PATCH, PUT");
    res.setHeader("Access-Control-Allow-Headers", "content-type");

    next()
})

app.use('/api', clientsRouter)
app.get('*', (req, res, next) => {
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'))
})

app.use(helmet())


mongodbConnect(() => {
    app.listen(port)
})

