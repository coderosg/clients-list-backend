const getDb = require('../utils/database').getDb
const mongodb = require('mongodb')

class Client {
    constructor(name, date, time, service, price, vk, phone, is_completed, created_at) {
        this.name = name
        this.date = date
        this.time = time
        this.service = service
        this.price = price
        this.vk = vk
        this.phone = phone
        this.is_completed = is_completed
        this.created_at = new Date()
    }

    save(id) {
        const db = getDb()
        let dbOp
        if (id) {
            dbOp = db.collection('clients')
                .updateOne({_id: new mongodb.ObjectId(id)}, {$set: this})
        } else {
            dbOp = db.collection('clients').insertOne(this)
        }
        return dbOp
            .then(result => {
                console.log(result);
            })
            .catch(err => {
                console.log(err);
            })
    }

    static fetchAll(page) {
        const db = getDb()

        return db.collection('clients')
            .find()
            .skip((page - 1) * 2)
            .limit(2)
            .toArray()
            .then(result => {
                return result
            })
            .catch(err => {
                throw err
            })
    }

    static fetchById(clientId) {
        const db = getDb()

        return db.collection('clients')
            .findOne({_id: new mongodb.ObjectId(clientId)})
            .then(client => {
                return client
            })
            .catch(err => {
                throw err
            })
    }

    static fetchByCompletion(isCompleted, page) {
        const db = getDb(),
            perPage = 3
        let pagesNumber
        db.collection('clients')
            .find({is_completed: isCompleted})
            .toArray()
            .then(result => {
                pagesNumber = Math.ceil(result.length / perPage)
            })
            .catch(err => {
                console.log(err);
            })
        return db.collection('clients')
            .find({is_completed: isCompleted})
            .skip((page - 1) * perPage)
            .limit(perPage)
            .toArray()
            .then(clients => {
                return {clients, pagesNumber}
            })
            .catch(err => {
                throw err
            })
    }

    static deleteById(clientId) {
        /*
            * find client
            * insert his to removed_clients collection
            * delete his from clients collection
         */
        const db = getDb()
        return db.collection('clients')
            .findOne({_id: new mongodb.ObjectId(clientId)})
            .then(client => {
                db.collection('removed_clients')
                    .insertOne(client)
                return db.collection('clients')
                    .deleteOne({_id: new mongodb.ObjectId(clientId)})
            })

            .catch(err => {
                console.log(err)
            })
    }

    static finishWork(clientId) {
        const db = getDb()
        return db.collection('clients')
            .updateOne({_id: new mongodb.ObjectId(clientId)}, {$set: {is_completed: true}})
            .then(client => {
                return client
            })
            .catch(err => {
                console.log(err)
            })
    }
}

module.exports = Client