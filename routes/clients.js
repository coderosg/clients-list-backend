const express = require('express');

const router = express.Router();

const clientsController = require('../controllers/clients');

router.get('/clients', clientsController.getClients);

router.get('/clients/:clientId', clientsController.getClient);

router.post('/add-client', clientsController.postClient);

router.post('/edit-client', clientsController.postClient);

router.delete('/clients/:clientId', clientsController.deleteClient);

router.patch('/clients/:clientId', clientsController.finishWork);

module.exports = router;